#include "util.h"
#include <ncurses.h>
#include <stdlib.h>

list_t *list_new(void *data) {
  list_t *node = malloc(sizeof(list_t));
  if (!node) {
    perror(__FILE__ " out of memory");
    return NULL;
  }
  node->next = NULL;
  node->prev = NULL;
  node->data = data;
  return node;
}

list_t *list_insert_after(list_t *node, void *data) {
  list_t *new_ = list_new(data);
  if (!new_)
    return NULL;
  new_->prev = node;
  if (node) {
    new_->next = node->next;
    node->next = new_;
    if (new_->next)
      new_->next->prev = new_;
  }
  return new_;
}

list_t *list_insert_before(list_t *node, void *data) {
  list_t *new_ = list_new(data);
  if (!new_)
    return NULL;
  new_->next = node;
  if (node) {
    new_->prev = node->prev;
    node->prev = new_;
    if (new_->prev)
      new_->prev->next = new_;
  }
  return new_;
}

void list_remove(list_t *node) {
  if (node->next)
    node->next->prev = node->prev;
  if (node->prev)
    node->prev->next = node->next;
  free(node);
}

void render_message(char *buf, size_t buflen, server_message_t *msg) {
  char *ptr = buf;
  size_t shift;
  struct tm *timestamp = localtime(&msg->timestamp);
  shift = snprintf(ptr, buflen, "[%02d:%02d:%02d] ", timestamp->tm_hour,
                   timestamp->tm_min, timestamp->tm_sec);
  ptr += shift;
  buflen -= shift;

  switch (msg->type) {
  case SMT_TEXT_MESSAGE: {
    shift = snprintf(ptr, buflen, "[%s]", msg->text.sender_name);
    ptr += shift;
    buflen -= shift;
    if (msg->text.receiver_name_len) {
      shift = snprintf(ptr, buflen, " -> [%s]", msg->text.receiver_name);
      ptr += shift;
      buflen -= shift;
    }
    shift = snprintf(ptr, buflen, ": %s\n", msg->text.text);
  } break;
  case SMT_JOIN_NOTIFICATION:
    shift = snprintf(ptr, buflen, "Member joined: [%s]\n", msg->member.name);
    break;
  case SMT_LEAVE_NOTIFICATION:
    shift = snprintf(ptr, buflen, "Member left: [%s]\n", msg->member.name);
    break;
  }
}

void display_message(WINDOW *window, server_message_t *msg) {
  static char buf[80 * 25 + 1];
  render_message(buf, sizeof(buf), msg);
  waddnstr(window, buf, sizeof(buf));
}
