#ifndef _UTIL_H_
#define _UTIL_H_

#include "net.h"
#include <errno.h>
#include <ncurses.h>
#include <stdio.h>
#include <string.h>

#define DBG_INT(action)                                                        \
  do {                                                                         \
    fprintf(stderr, "[%s:%d]: %s = %d\n", __FILE__, __LINE__, #action,         \
            (action));                                                         \
  } while (0)

#define CHKERR(action)                                                         \
  do {                                                                         \
    int retcode_##__LINE__ = action;                                           \
    if (retcode_##__LINE__ < 0) {                                              \
      int errno_##__LINE__ = errno;                                            \
      fprintf(stderr, "[%s:%d]: error %d (%s)\n", __FILE__, __LINE__,          \
              errno_##__LINE__, strerror(errno_##__LINE__));                   \
      return retcode_##__LINE__;                                               \
    }                                                                          \
  } while (0)

#define CHKERR_PTHREAD(action)                                                 \
  do {                                                                         \
    int retcode_##__LINE__ = action;                                           \
    if (retcode_##__LINE__) {                                                  \
      fprintf(stderr, "[%s:%d]: %s", __FILE__, __LINE__, #action);             \
      return -1;                                                               \
    }                                                                          \
  } while (0);

#define MY_ROUTINE(name, arg)                                                  \
  static int name(arg);                                                        \
  static void *name##_voidptr(void *ptr) {                                     \
    name(ptr);                                                                 \
    return NULL;                                                               \
  }                                                                            \
  static int name(arg)

typedef struct list {
  struct list *next;
  struct list *prev;
  void *data;
} list_t;

list_t *list_new(void *data);
list_t *list_insert_after(list_t *node, void *data);
list_t *list_insert_before(list_t *node, void *data);
void list_remove(list_t *node);
void render_message(char *buf, size_t buflen, server_message_t *msg);
void display_message(WINDOW *window, server_message_t *msg);

#endif
