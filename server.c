#include "server.h"
#include "net.h"
#include "server_net.h"
#include "util.h"
#include <memory.h>
#include <ncurses.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <sys/socket.h>

typedef struct server_shared_data {
  pthread_cond_t queue_cond;
  pthread_mutex_t connection_mutex;
  pthread_mutex_t history_mutex;
  pthread_mutex_t queue_mutex;
  list_t *connections;
  list_t *message_queue_head;
  list_t *message_queue_tail;
  list_t *message_history;
  pthread_t acceptor_thread;
  pthread_t queue_thread;
  int sock;
  atomic_int is_running;
} server_shared_data_t;

typedef struct server_connection_data {
  pthread_t thread;
  server_shared_data_t *shared;
  list_t *list_pos;
  char *client_name;
  uint32_t client_name_len;
  atomic_int is_active;
  int conn;
} server_connection_data_t;

MY_ROUTINE(queue_routine, server_shared_data_t *shared) {
  uint32_t id = 0;
  scrollok(stdscr, TRUE);

  pthread_mutex_lock(&shared->queue_mutex);
  while (atomic_load(&shared->is_running)) {
    pthread_cond_wait(&shared->queue_cond, &shared->queue_mutex);
    while (shared->message_queue_head) {
      server_message_t *msg = shared->message_queue_head->data;

      msg->timestamp = time(NULL);
      msg->id = ++id;

      pthread_mutex_lock(&shared->history_mutex);
      shared->message_history = list_insert_after(shared->message_history, msg);
      pthread_mutex_unlock(&shared->history_mutex);

      display_message(stdscr, msg);
      refresh();

      list_t *iter;
      for (iter = shared->connections; iter; iter = iter->prev) {
        server_connection_data_t *cd = iter->data;
        if (atomic_load(&cd->is_active)) {
          if (msg->type != SMT_TEXT_MESSAGE || !msg->text.receiver_name_len ||
              !strcmp(msg->text.receiver_name, cd->client_name) ||
              !strcmp(msg->text.sender_name, cd->client_name))
            CHKERR(send_server_message(cd->conn, msg, MSG_DONTWAIT));
        }
      }

      shared->message_queue_head = shared->message_queue_head->next;
      if (shared->message_queue_head)
        list_remove(shared->message_queue_head->prev);
    }
  }
  pthread_mutex_unlock(&shared->queue_mutex);
  return 0;
}

static void queue_append(server_shared_data_t *shared, server_message_t *msg) {
  pthread_mutex_lock(&shared->queue_mutex);
  shared->message_queue_tail =
      list_insert_after(shared->message_queue_tail, msg);
  if (!shared->message_queue_head)
    shared->message_queue_head = shared->message_queue_tail;
  pthread_mutex_unlock(&shared->queue_mutex);
  pthread_cond_signal(&shared->queue_cond);
}

MY_ROUTINE(connection_routine, server_connection_data_t *self) {
  int retcode = 1;
  while (retcode > 0 && atomic_load(&self->is_active) &&
         atomic_load(&self->shared->is_running)) {
    client_text_message_t cmsg;
    retcode = receive_client_text_message(self->conn, &cmsg);
    server_message_t *msg = malloc(sizeof(server_message_t));
    switch (retcode) {
    case 1:
      msg->type = SMT_TEXT_MESSAGE;
      msg->text.sender_name = self->client_name;
      msg->text.sender_name_len = self->client_name_len;
      msg->text.receiver_name = cmsg.receiver_name;
      msg->text.receiver_name_len = cmsg.receiver_name_len;
      msg->text.text = cmsg.text;
      msg->text.text_len = cmsg.text_len;
      queue_append(self->shared, msg);
      break;
    case 0:
      msg->type = SMT_LEAVE_NOTIFICATION;
      msg->member.name = self->client_name;
      msg->member.length = self->client_name_len;
      queue_append(self->shared, msg);
      break;
    case -1:
      free(msg);
      break;
    }
  }

  atomic_store(&self->is_active, 0);
  return retcode;
}

MY_ROUTINE(acceptor_routine, server_shared_data_t *shared) {
  while (atomic_load(&shared->is_running)) {
    int conn;
    CHKERR(conn = accept_connection(shared->sock));
    server_connection_data_t *data = malloc(sizeof(server_connection_data_t));
    int received_name =
        receive_string(conn, &data->client_name_len, &data->client_name);
    if (received_name == 1) {
      pthread_mutex_lock(&shared->history_mutex);

      data->shared = shared;
      data->conn = conn;
      atomic_store(&data->is_active, 1);
      CHKERR_PTHREAD(pthread_create(&data->thread, NULL,
                                    connection_routine_voidptr, data));

      pthread_mutex_lock(&shared->connection_mutex);
      shared->connections = data->list_pos =
          list_insert_after(shared->connections, data);
      pthread_mutex_unlock(&shared->connection_mutex);

      list_t *iter;
      for (iter = shared->message_history; iter; iter = iter->prev)
        send_server_message(conn, iter->data, MSG_DONTWAIT);
      pthread_mutex_unlock(&shared->history_mutex);

      server_message_t *msg = malloc(sizeof(server_message_t));
      msg->type = SMT_JOIN_NOTIFICATION;
      msg->member.length = data->client_name_len;
      msg->member.name = data->client_name;
      queue_append(shared, msg);
    } else {
      free(data);
    }
  }
  return 0;
}

int run_server(int argc, char **argv) {
  scrollok(stdscr, TRUE);
  if (argc < 3)
    return -1;
  server_shared_data_t shared;
  CHKERR(shared.sock = init_server_socket(atoi(argv[2])));

  CHKERR_PTHREAD(pthread_cond_init(&shared.queue_cond, NULL));
  CHKERR_PTHREAD(pthread_mutex_init(&shared.connection_mutex, NULL));
  CHKERR_PTHREAD(pthread_mutex_init(&shared.history_mutex, NULL));
  CHKERR_PTHREAD(pthread_mutex_init(&shared.queue_mutex, NULL));

  shared.connections = NULL;
  shared.message_queue_head = NULL;
  shared.message_queue_tail = NULL;
  shared.message_history = NULL;

  atomic_store(&shared.is_running, 1);
  CHKERR_PTHREAD(pthread_create(&shared.acceptor_thread, NULL,
                                acceptor_routine_voidptr, &shared));
  CHKERR_PTHREAD(pthread_create(&shared.queue_thread, NULL,
                                queue_routine_voidptr, &shared));

  int c;
  do {
    c = getch();
  } while (c != 'q' && c != 'Q');

  atomic_store(&shared.is_running, 0);
  pthread_cond_signal(&shared.queue_cond);
  pthread_join(shared.queue_thread, NULL);

  while (shared.connections) {
    list_t *prev = shared.connections->prev;
    server_connection_data_t *data = shared.connections->data;
    atomic_store(&data->is_active, 0);
    free_connection(data->conn);
    pthread_join(data->thread, NULL);
    free(data->client_name);
    free(data);
    list_remove(shared.connections);
    shared.connections = prev;
  }

  free_connection(shared.sock);
  pthread_join(shared.acceptor_thread, NULL);

  pthread_mutex_destroy(&shared.connection_mutex);
  pthread_mutex_destroy(&shared.history_mutex);
  pthread_mutex_destroy(&shared.queue_mutex);

  return 0;
}
