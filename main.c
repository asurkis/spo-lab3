#include "client.h"
#include "server.h"
#include <ncurses.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    return -1;
  }

  initscr();
  // raw();
  cbreak();
  noecho();
  int curs_state = curs_set(0);
  int retcode = (argv[1][0] == 'c' ? run_client : run_server)(argc, argv);
  curs_set(curs_state);
  endwin();
  return retcode;
}
