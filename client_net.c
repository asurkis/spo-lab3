#include "client_net.h"
#include "util.h"
#include <arpa/inet.h>
#include <endian.h>
#include <inttypes.h>
#include <memory.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

int init_client_socket(char const *address, unsigned short port) {
  int sock;
  CHKERR(sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP));

  struct sockaddr_in addr = {0};
  addr.sin_family = PF_INET;
  addr.sin_port = htons(port);

  switch (inet_pton(PF_INET, address, &addr.sin_addr)) {
  case 0:
    perror(__FILE__ " incorrect address");
    return -1;
  case -1:
    return -1;
  case 1:
    break;
  }

  if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror(__FILE__ " connect()");
    close(sock);
    return -1;
  }

  return sock;
}

int send_client_text_message(int sock, client_text_message_t *msg) {
  CHKNET(
      send_string(sock, msg->receiver_name_len, msg->receiver_name, MSG_MORE));
  CHKNET(send_string(sock, msg->text_len, msg->text, 0));
  return 1;
}

static int receive_server_text_message(int sock, server_text_message_t *out) {
  CHKNET(receive_string(sock, &out->sender_name_len, &out->sender_name));
  CHKNET(receive_string(sock, &out->receiver_name_len, &out->receiver_name));
  CHKNET(receive_string(sock, &out->text_len, &out->text));
  return 1;
}

static int
receive_server_member_notification(int sock,
                                   server_member_notification_t *out) {
  CHKNET(receive_string(sock, &out->length, &out->name));
  return 1;
}

int receive_server_message(int sock, server_message_t *out) {
  int64_t timestamp;
  CHKNET(recv(sock, &timestamp, sizeof(int64_t), MSG_WAITALL));
  CHKNET(recv(sock, &out->id, sizeof(uint32_t), MSG_WAITALL));
  NTOHL(out->id);
  out->timestamp = be64toh(timestamp);

  uint8_t type;
  CHKNET(recv(sock, &type, sizeof(uint8_t), MSG_WAITALL));
  out->type = type;

  switch (type) {
  case SMT_TEXT_MESSAGE:
    CHKNET(receive_server_text_message(sock, &out->text));
    break;
  case SMT_JOIN_NOTIFICATION:
  case SMT_LEAVE_NOTIFICATION:
    CHKNET(receive_server_member_notification(sock, &out->member));
    break;
  default:
    fprintf(stderr, "Unknown type: %" PRIu8 "\n", type);
    return -1;
  }
  return 1;
}
