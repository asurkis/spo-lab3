#include "net.h"
#include "util.h"
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

int free_connection(int sock) {
  int shutdown_retcode = shutdown(sock, SHUT_RDWR);
  CHKERR(close(sock));
  CHKERR(shutdown_retcode);
  return 0;
}

int send_string(int sock, uint32_t len, char *str, int flags) {
  uint32_t len_to_send = htonl(len);
  CHKERR(send(sock, &len_to_send, sizeof(uint32_t), flags | MSG_MORE));
  CHKERR(send(sock, str, len, flags));
  return 1;
}

int receive_string(int sock, uint32_t *len_out, char **str_out) {
  CHKNET(recv(sock, len_out, sizeof(uint32_t), MSG_WAITALL));
  NTOHL(*len_out);
  *str_out = malloc(*len_out + 1);
  if (!*str_out) {
    perror(__FILE__ " out of memory");
    return -1;
  }
  if (*len_out)
    CHKNET(recv(sock, *str_out, *len_out, MSG_WAITALL));
  str_out[0][*len_out] = 0;
  return 1;
}
