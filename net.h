#ifndef _MESSAGES_H_
#define _MESSAGES_H_

#include <inttypes.h>
#include <time.h>

typedef enum server_message_type {
  SMT_TEXT_MESSAGE,
  SMT_JOIN_NOTIFICATION,
  SMT_LEAVE_NOTIFICATION
} server_message_type_t;

typedef struct client_text_message {
  uint32_t receiver_name_len;
  uint32_t text_len;
  char *receiver_name;
  char *text;
} client_text_message_t;

typedef struct server_text_message {
  uint32_t sender_name_len;
  uint32_t receiver_name_len;
  uint32_t text_len;
  char *sender_name;
  char *receiver_name;
  char *text;
} server_text_message_t;

typedef struct server_member_notification {
  uint32_t length;
  char *name;
} server_member_notification_t;

typedef struct server_message {
  time_t timestamp;
  uint32_t id;
  server_message_type_t type;
  union {
    server_member_notification_t member;
    server_text_message_t text;
  };
} server_message_t;

#define HTONL(x) ((x) = htonl(x))
#define NTOHL(x) ((x) = ntohl(x))
#define HTONLL(x) ((x) = htobe64(x))
#define NTOHLL(x) ((x) = be64toh(x))

#define CHKNET(action)                                                         \
  do {                                                                         \
    int retcode_##__LINE__ = action;                                           \
    if (retcode_##__LINE__ < 0) {                                              \
      int errno_##__LINE__ = errno;                                            \
      fprintf(stderr, "[%s:%d]: error %d (%s)\n", __FILE__, __LINE__,          \
              errno_##__LINE__, strerror(errno_##__LINE__));                   \
      return retcode_##__LINE__;                                               \
    } else if (retcode_##__LINE__ == 0) {                                      \
      return 0;                                                                \
    }                                                                          \
  } while (0)

int free_connection(int sock);
int send_string(int sock, uint32_t len, char *str, int flags);
int receive_string(int sock, uint32_t *len_out, char **str_out);

#endif
