#include "server_net.h"
#include "util.h"
#include <arpa/inet.h>
#include <endian.h>
#include <inttypes.h>
#include <memory.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

int init_server_socket(unsigned short port) {
  int retcode = -1;
  int sock;
  CHKERR(sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP));

  struct sockaddr_in addr = {0};
  addr.sin_family = PF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror(__FILE__ " bind()");
    CHKERR(close(sock));
    return -1;
  }

  if (listen(sock, SOMAXCONN) == -1) {
    perror(__FILE__ " listen()");
    CHKERR(close(sock));
    return -1;
  }

  return sock;
}

int accept_connection(int sock) {
  int connection;
  CHKERR(connection = accept(sock, NULL, NULL));
  return connection;
}

int send_server_text_message(int sock, server_text_message_t *msg, int flags) {
  CHKNET(send_string(sock, msg->sender_name_len, msg->sender_name,
                     flags | MSG_MORE));
  CHKNET(send_string(sock, msg->receiver_name_len, msg->receiver_name,
                     flags | MSG_MORE));
  CHKNET(send_string(sock, msg->text_len, msg->text, flags));
  return 1;
}

int send_server_message(int sock, server_message_t *msg, int flags) {
  int64_t timestamp = htobe64(msg->timestamp);
  uint32_t id = htonl(msg->id);
  uint8_t type = msg->type;
  CHKNET(send(sock, &timestamp, sizeof(int64_t), MSG_MORE));
  CHKNET(send(sock, &id, sizeof(uint32_t), MSG_MORE));
  CHKNET(send(sock, &type, sizeof(uint8_t), MSG_MORE));
  switch (msg->type) {
  case SMT_TEXT_MESSAGE:
    CHKNET(send_server_text_message(sock, &msg->text, flags));
    break;
  case SMT_JOIN_NOTIFICATION:
  case SMT_LEAVE_NOTIFICATION:
    CHKNET(send_string(sock, msg->member.length, msg->member.name, flags));
    break;
  }
  return 1;
}

int receive_client_text_message(int sock, client_text_message_t *out) {
  CHKNET(receive_string(sock, &out->receiver_name_len, &out->receiver_name));
  CHKNET(receive_string(sock, &out->text_len, &out->text));
  return 1;
}
